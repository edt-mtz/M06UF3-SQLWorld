package world;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class UserHandler extends DefaultHandler {

    private static boolean country = false;
    private boolean countryName = false;

    private static boolean city = false;
    private static boolean cityName = false;

    private static String countryNotEquals = "";
    private static String newCountry;

    public static ArrayList <String> duplicateCities = new ArrayList<>();

    private static HashSet<String> citySet = new HashSet<>();
    private static HashMap<String, ArrayList<String>> finalMapParser = new HashMap<>();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {

        if (qName.equalsIgnoreCase("country")) {
            country = true;
        }

        if (country && qName.equalsIgnoreCase("name")) {
            country = false;
            countryName = true;
        }

        if (qName.equalsIgnoreCase("city")) {
            city = true;
        }

        if (city && qName.equalsIgnoreCase("name")) {
            city = false;
            cityName = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        /*if (qName.equalsIgnoreCase("country")) {
        }*/
    }

    @Override
    public void characters(char ch[], int start, int length) throws SAXException {
        if (countryName) {

            newCountry = new String(ch, start, length);

            if (newCountry.compareToIgnoreCase(countryNotEquals) == 0) {

            } else if (countryNotEquals != "") {
                ArrayList <String>cloneCitySet = new ArrayList<>();
                cloneCitySet.addAll(citySet);
                finalMapParser.put(countryNotEquals, cloneCitySet);
                citySet.clear();
            }
            countryNotEquals = newCountry;
        }

        countryName = false;
        if (cityName) {
            String ct = new String(ch, start, length);
            if (citySet.contains(ct)) {
                duplicateCities.add(ct);
            } else {
                citySet.add(ct);
            }
        }
        cityName = false;
    }
    public HashMap<String, ArrayList<String>> getFinalMap() {
        return finalMapParser;
    }
}