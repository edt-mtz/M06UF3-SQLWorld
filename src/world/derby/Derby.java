package world.derby;

import org.apache.derby.shared.common.error.DerbySQLIntegrityConstraintViolationException;

import java.sql.*;

import static world.MainWorld.*;
import static world.UserHandler.duplicateCities;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Derby {
    // Derby
    private static String dbDerby = "derby";
    private static String db_derby_Name = "localdb";
    private static String derbyCreatrTrue = "create=true";


    public static void menuJavaDb() {
        System.out.println("\n**********************************************");
        System.out.println("*                   JavaDB                   *");
        System.out.println("**********************************************");
        System.out.println(String.format("[1] Create/Reset tables: %s and %s", COUNTRY_TABLE, CITY_TABLE));
        System.out.println(String.format("[2] Insert %s & %s from XML", COUNTRY_TABLE, CITY_TABLE));
        System.out.println(String.format("[3] Insert a new %s", COUNTRY_TABLE_FIELD));
        System.out.println(String.format("[4] Insert a new %s", CITY_TABLE_FIELD));
        System.out.println("**********************************************");
        System.out.println(String.format("[5] Remove a %s", COUNTRY_TABLE_FIELD));
        System.out.println(String.format("[6] Remove a %s", CITY_TABLE_FIELD));
        System.out.println("**********************************************");
        System.out.println(String.format("[7] Update a %s", COUNTRY_TABLE_FIELD));
        System.out.println(String.format("[8] Update a %s", CITY_TABLE_FIELD));
        System.out.println("**********************************************");
        System.out.println(String.format("[9] Show all %s and %s", CITY_TABLE, COUNTRY_TABLE));
        System.out.println("**********************************************");
        System.out.println("[b] Return back");
        System.out.println("**********************************************\n");
        System.out.print("Select: ");
    }

    public static void derbyDb() {

        Connection connEmb = null;

        try {
            connEmb = DriverManager.getConnection("jdbc:" + dbDerby + ":" + db_derby_Name + ";" + derbyCreatrTrue);
            connEmb.setAutoCommit(false);
            menuJavaDb();

            long lines;
            String ct;
            String cnt;

            do {
                menuItem = scan.nextLine();

                switch (menuItem) {

                    case "1":
                        Statement statementEmb = connEmb.createStatement();
                        try {
                            // Drop tables
                            statementEmb.executeUpdate(String.format("DROP TABLE %s", CITY_TABLE));
                            statementEmb.executeUpdate(String.format("DROP TABLE %s", COUNTRY_TABLE));
                            statementEmb.executeUpdate(String.format("DROP SEQUENCE %s_SEQ RESTRICT", COUNTRY_TABLE));

                        } catch (SQLSyntaxErrorException psql) {
                            System.out.println("Tables don't exist");
                        }

                        // Countries table
                        statementEmb.executeUpdate(String.format("CREATE TABLE %s ( "
                                        + "%s" + " integer NOT NULL CONSTRAINT PK_%s_ID PRIMARY KEY,"
                                        + "%s" + " varchar(255) NOT NULL," +
                                        "CONSTRAINT %s_%s_UK UNIQUE(%s))", COUNTRY_TABLE, COUNTRY_TABLE_FIELD, CITY_TABLE_FIELD
                                , TABLE_NAME_FIELD, COUNTRY_TABLE, TABLE_NAME_FIELD, TABLE_NAME_FIELD));

                        // Cities table
                        statementEmb.executeUpdate(String.format("CREATE TABLE %s ( "
                                        + "%s" + " integer ,"
                                        + "%s" + " varchar(255) NOT NULL,"
                                        + "CONSTRAINT PK_%s_FP PRIMARY KEY(%s,%s),"
                                        + "CONSTRAINT FK_%s_%s FOREIGN KEY(%s) REFERENCES %s(%s) "
                                        + " ON DELETE CASCADE)"
                                , CITY_TABLE, COUNTRY_TABLE_FIELD, TABLE_NAME_FIELD, CITY_TABLE, COUNTRY_TABLE_FIELD, TABLE_NAME_FIELD
                                , CITY_TABLE, COUNTRY_TABLE_FIELD, COUNTRY_TABLE_FIELD, COUNTRY_TABLE, COUNTRY_TABLE_FIELD));

                        // Sequence
                        statementEmb.executeUpdate(String.format("CREATE SEQUENCE %s_SEQ AS INT MAXVALUE 999999 CYCLE", COUNTRY_TABLE));

                        connEmb.commit();
                        statementEmb.close();

                        System.out.println(String.format("Tables: %s & %s successfully created", COUNTRY_TABLE, CITY_TABLE));

                        showMenu();
                        break;
                    case "2":
                        saxParser();

                        PreparedStatement preparedStatementCountryEmb = connEmb.prepareStatement(insertCountriesSqlDerby);
                        PreparedStatement preparedStatementCityEmb = connEmb.prepareStatement(insertCitiesSql);

                        for (String country : finalMap.keySet()) {
                            preparedStatementCountryEmb.setString(1, country);
                            preparedStatementCountryEmb.addBatch();

                            for (int i = 0; i < finalMap.get(country).size(); i++) {
                                String city = finalMap.get(country).get(i);
                                preparedStatementCityEmb.setString(1, country);
                                preparedStatementCityEmb.setString(2, city);
                                preparedStatementCityEmb.addBatch();
                            }
                        }

                        long var = 0;

                        try {

                            int[] ctrarray = preparedStatementCountryEmb.executeBatch();

                            for (int z : ctrarray) {
                                var += z;
                            }

                            int[] ctarray = preparedStatementCityEmb.executeBatch();

                            for (int i : ctarray) {
                                var += i;
                            }

                            connEmb.commit();
                            preparedStatementCountryEmb.close();
                            preparedStatementCityEmb.close();
                            lines = var;
                            System.out.println(String.format("Duplicate %s: " + duplicateCities, CITY_TABLE));
                            duplicateCities.clear();
                        } catch (BatchUpdateException bue) {
                            lines = 0;
                        }

                        System.out.println("Changes: " + lines);

                        finalMap.clear();
                        showMenu();
                        break;
                    case "3":
                        System.out.print(String.format("Type a %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());

                        PreparedStatement preparedInsertCountryEmb = connEmb.prepareStatement(insertCountriesSqlDerby);
                        preparedInsertCountryEmb.setString(1, cnt);

                        try {
                            lines = preparedInsertCountryEmb.executeUpdate();
                        } catch (DerbySQLIntegrityConstraintViolationException derbysql) {
                            lines = 0;
                        } catch (SQLException psql) {
                            lines = 0;
                        }

                        connEmb.commit();

                        preparedInsertCountryEmb.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "4":
                        System.out.print(String.format("Type a %s name: ", CITY_TABLE_FIELD));
                        ct = toInitCap(scan.nextLine().toLowerCase());

                        System.out.print(String.format("Type a %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());

                        PreparedStatement preparedInsertCity = connEmb.prepareStatement(insertCitiesSql);
                        preparedInsertCity.setString(1, cnt);
                        preparedInsertCity.setString(2, ct);

                        try {
                            lines = preparedInsertCity.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        connEmb.commit();

                        preparedInsertCity.close();

                        System.out.println("Changes: " + lines);


                        showMenu();
                        break;
                    case "5":
                        System.out.print(String.format("Type a %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());


                        PreparedStatement preparedDeleteCountry = connEmb.prepareStatement(deleteCountriesSql);
                        preparedDeleteCountry.setString(1, cnt);

                        try {
                            lines = preparedDeleteCountry.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        connEmb.commit();

                        preparedDeleteCountry.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "6":
                        System.out.print(String.format("Type a %s name: ", CITY_TABLE_FIELD));
                        ct = toInitCap(scan.nextLine().toLowerCase());

                        System.out.print(String.format("Type a %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());

                        PreparedStatement preparedDeleteCity = connEmb.prepareStatement(deleteCitiesSql);
                        preparedDeleteCity.setString(1, ct);
                        preparedDeleteCity.setString(2, cnt);

                        try {
                            lines = preparedDeleteCity.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        connEmb.commit();

                        preparedDeleteCity.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "7":
                        System.out.print(String.format("Type the old %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());


                        System.out.print(String.format("Type the new %s name: ", COUNTRY_TABLE_FIELD));
                        String cntNew = toInitCap(scan.nextLine().toLowerCase());

                        PreparedStatement preparedUpdateCountry = connEmb.prepareStatement(updateCountriesSql);
                        preparedUpdateCountry.setString(1, cntNew);
                        preparedUpdateCountry.setString(2, cnt);

                        try {
                            lines = preparedUpdateCountry.executeUpdate();
                        } catch (SQLException sql) {
                            lines = 0;
                        }

                        connEmb.commit();

                        preparedUpdateCountry.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "8":
                        System.out.print(String.format("Type the old %s name: ", CITY_TABLE_FIELD));
                        ct = toInitCap(scan.nextLine().toLowerCase());

                        System.out.print(String.format("Type its %s name: ", COUNTRY_TABLE_FIELD));
                        cnt = toInitCap(scan.nextLine().toLowerCase());


                        System.out.print(String.format("Type the new %s name: ", CITY_TABLE_FIELD));
                        String ctNew = toInitCap(scan.nextLine().toLowerCase());


                        PreparedStatement preparedUpdateCity = connEmb.prepareStatement(updateCitiesSql);
                        preparedUpdateCity.setString(1, ctNew);
                        preparedUpdateCity.setString(2, ct);
                        preparedUpdateCity.setString(3, cnt);

                        try {
                            lines = preparedUpdateCity.executeUpdate();
                        } catch (SQLException sql) {

                            lines = 0;
                        }

                        connEmb.commit();

                        preparedUpdateCity.close();

                        System.out.println("Changes: " + lines);

                        showMenu();
                        break;
                    case "9":
                        Statement statement = connEmb.createStatement();

                        String selectDb = String.format("SELECT %s.%s AS %s, %s.%s AS %s " +
                                        "FROM %s,%s WHERE %s.%s=%s.%s " +
                                        "ORDER BY %s.%s, %s.%s", COUNTRY_TABLE, TABLE_NAME_FIELD, COUNTRY_TABLE_FIELD, CITY_TABLE, TABLE_NAME_FIELD
                                , CITY_TABLE_FIELD, COUNTRY_TABLE, CITY_TABLE, COUNTRY_TABLE, COUNTRY_TABLE_FIELD, CITY_TABLE, COUNTRY_TABLE_FIELD
                                , COUNTRY_TABLE, TABLE_NAME_FIELD, CITY_TABLE, TABLE_NAME_FIELD);

                        ResultSet rsQuery = statement.executeQuery(selectDb);

                        if (!select.isEmpty()) {
                            select.clear();
                        }

                        while (rsQuery.next()) {
                            String[] arr = new String[2];

                            arr[0] = rsQuery.getString(String.format("%s", COUNTRY_TABLE_FIELD));
                            arr[1] = rsQuery.getString(String.format("%s", CITY_TABLE_FIELD));

                            select.add(arr);
                        }
                        rsQuery.close();

                        int maxA = 0;
                        int maxB = 0;
                        for (String[] arr : select) {
                            if (arr[0].length() > maxA) {
                                maxA = arr[0].length();
                            }
                            if (arr[1].length() > maxB) {
                                maxB = arr[1].length();
                            }
                        }

                        if (select.isEmpty()) {
                            showMenu();
                            break;
                        }

                        select.clear();

                        String bar = "";
                        for (int i = 0; i < maxA + maxB; i++) {
                            bar += "-";
                        }

                        ResultSet rsQueryFinal = statement.executeQuery(selectDb);

                        System.out.println(bar);

                        System.out.println(String.format("%-" + maxA + "s | %-" + maxB + "s", String.format("%s", COUNTRY_TABLE_FIELD)
                                , String.format("%s", CITY_TABLE_FIELD)));

                        System.out.println(bar);

                        while (rsQueryFinal.next()) {
                            System.out.println(String.format("%-" + maxA + "s | %-" + maxB + "s", rsQueryFinal.getString(String.format("%s", COUNTRY_TABLE_FIELD))
                                    , rsQueryFinal.getString(String.format("%s", CITY_TABLE_FIELD))));
                        }
                        rsQueryFinal.close();

                        statement.close();

                        System.out.println(bar);

                        showMenu();
                        break;
                    /*case "10":

                        selectCountryCityList = new HashMap<>();

                        if (!selectCountryCityList.isEmpty()) {
                            selectCountryCityList.clear();
                        }

                        statement = connEmb.createStatement();

                        String selectDb1 = String.format("SELECT %s AS %s " +
                                        "FROM %s"
                                , TABLE_NAME_FIELD, COUNTRY_TABLE_FIELD, COUNTRY_TABLE);

                        String selectDb2 = String.format("SELECT %s.%s AS %s, %s.%s AS %s " +
                                        "FROM %s,%s WHERE %s.%s=%s.%s " +
                                        "ORDER BY %s.%s, %s.%s", COUNTRY_TABLE, TABLE_NAME_FIELD, COUNTRY_TABLE_FIELD, CITY_TABLE, TABLE_NAME_FIELD
                                , CITY_TABLE_FIELD, COUNTRY_TABLE, CITY_TABLE, COUNTRY_TABLE, COUNTRY_TABLE_FIELD, CITY_TABLE, COUNTRY_TABLE_FIELD
                                , COUNTRY_TABLE, TABLE_NAME_FIELD, CITY_TABLE, TABLE_NAME_FIELD);

                        ResultSet rsQuery1 = statement.executeQuery(selectDb1);
                        ResultSet rsQuery2 = statement.executeQuery(selectDb2);

                        while (rsQuery1.next()) {

                            String column1 = rsQuery1.getString(String.format("%s", COUNTRY_TABLE_FIELD));

                            while (rsQuery2.next()) {

                            }
                        }
                        rsQuery1.close();
                        rsQuery2.close();

                        Connection connPostgres = DriverManager.getConnection("jdbc:" + dbPostgres + "://" + dbHost + "/" + db, "postgres", "jupiter");

                        connPostgres.setAutoCommit(false);

                        PreparedStatement preparedStatementCountry = connPostgres.prepareStatement(insertCountriesSql);
                        PreparedStatement preparedStatementCity = connPostgres.prepareStatement(insertCitiesSql);

                        for (String country : finalMap.keySet()) {
                            preparedStatementCountry.setString(1, country);
                            preparedStatementCountry.addBatch();

                            for (int i = 0; i < finalMap.get(country).size(); i++) {
                                String city = finalMap.get(country).get(i);
                                preparedStatementCity.setString(1, country);
                                preparedStatementCity.setString(2, city);
                                preparedStatementCity.addBatch();
                            }
                        }

                        var = 0;

                        try {

                            int[] ctrarray = preparedStatementCountry.executeBatch();

                            for (int z : ctrarray) {
                                var += z;
                            }

                            int[] ctarray = preparedStatementCity.executeBatch();

                            for (int i : ctarray) {
                                var += i;
                            }

                            connPostgres.commit();
                            preparedStatementCountry.close();
                            preparedStatementCity.close();
                            lines = var;
                            System.out.println(String.format("Duplicate %s: " + duplicateCities, CITY_TABLE));
                            duplicateCities.clear();
                        } catch (BatchUpdateException bue) {
                            lines = 0;
                        }

                        System.out.println("Changes: " + lines);

                        selectCountryCityList.clear();

                        connPostgres.close();

                        showMenu();
                        break;*/
                    case "m":
                        menuJavaDb();
                        break;
                    case "b":
                        mainMenu();
                        break;
                    default:
                        showMenu();
                        break;
                }
            } while (!menuItem.equals("b"));

        } catch (SQLException sql) {
            System.err.println(sql.getErrorCode());
            sql.printStackTrace();
        } finally {
            if (connEmb != null) {
                try {
                    connEmb.rollback();
                    connEmb.close();
                } catch (SQLException sql) {
                    System.err.println(sql.getErrorCode());
                    sql.printStackTrace();
                }
            }
        }
    }

}
