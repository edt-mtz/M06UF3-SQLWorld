package world;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.*;


import static world.derby.Derby.derbyDb;
import static world.postgres.Postgres.db;
import static world.postgres.Postgres.postgreSql;

/**
 *
 * Copyright 2017 ManuMtz
 *
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class MainWorld {

    public static Scanner scan = new Scanner(System.in);
    public static String menuItem;

    // Constants
    public static final String TABLE_NAME_FIELD = "name";
    public static final String COUNTRY_TABLE = "countries";
    public static final String CITY_TABLE = "cities";

    public static final String COUNTRY_TABLE_FIELD = "country";
    public static final String CITY_TABLE_FIELD = "city";

    public static HashMap<String, ArrayList<String>> finalMap;

    public static ArrayList<String[]> select = new ArrayList<>();

    // Selects
    public static String selectCountry = String.format("SELECT %s FROM %s WHERE %s=?", TABLE_NAME_FIELD
            , COUNTRY_TABLE, TABLE_NAME_FIELD);

    public static String selectCity = String.format("SELECT %s FROM %s WHERE %s=? " +
                    "AND %s =(SELECT %s FROM %s WHERE %s=?)", TABLE_NAME_FIELD, CITY_TABLE, TABLE_NAME_FIELD
            , COUNTRY_TABLE_FIELD, CITY_TABLE_FIELD, COUNTRY_TABLE, TABLE_NAME_FIELD);

    // Inserts
    public static String insertCountriesSql = String.format("INSERT INTO %s (%s) VALUES (?)", COUNTRY_TABLE
            , TABLE_NAME_FIELD);

    public static String insertCountriesSqlDerby = String.format("INSERT INTO %s (%s,%s) VALUES (NEXT VALUE FOR %s_SEQ,?)", COUNTRY_TABLE
            , COUNTRY_TABLE_FIELD, TABLE_NAME_FIELD, COUNTRY_TABLE);

    public static String insertCitiesSql = String.format("INSERT INTO %s VALUES " +
                    "((SELECT %s FROM %s WHERE %s=?),?)", CITY_TABLE, COUNTRY_TABLE_FIELD
            , COUNTRY_TABLE, TABLE_NAME_FIELD);

    // Deletes
    public static String deleteCountriesSql = String.format("DELETE FROM %s WHERE %s=?"
            , COUNTRY_TABLE, TABLE_NAME_FIELD);

    public static String deleteCitiesSql = String.format("DELETE FROM %s WHERE %s=? " +
                    "AND %s=(SELECT %s FROM %s WHERE %s=?)", CITY_TABLE, TABLE_NAME_FIELD
            , COUNTRY_TABLE_FIELD, COUNTRY_TABLE_FIELD, COUNTRY_TABLE, TABLE_NAME_FIELD);

    // Updates
    public static String updateCountriesSql = String.format("UPDATE %s SET %s=? WHERE %s=?"
            , COUNTRY_TABLE, TABLE_NAME_FIELD, TABLE_NAME_FIELD);

    public static String updateCitiesSql = String.format("UPDATE %s SET %s=? WHERE %s=? " +
                    "AND %s=(SELECT %s FROM %s WHERE %s=?)"
            , CITY_TABLE, TABLE_NAME_FIELD, TABLE_NAME_FIELD, COUNTRY_TABLE_FIELD, COUNTRY_TABLE_FIELD
            , COUNTRY_TABLE, TABLE_NAME_FIELD);


    public static void saxParser() {
        try {
            finalMap = new HashMap<>();
            File inputFile = new File("data/" + db + ".xml");
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            UserHandler userhandler = new UserHandler();
            saxParser.parse(inputFile, userhandler);
            finalMap = userhandler.getFinalMap();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public static void mainMenu() {
        System.out.println("\n**********************************************");
        System.out.println("*         Manuel Martínez Martín - DB        *");
        System.out.println("**********************************************");
        System.out.println("[1] Connect to Postgres");
        System.out.println("[2] Connect to Derby");
        System.out.println("**********************************************");
        System.out.println("[q] Exit");
        System.out.println("**********************************************\n");
        System.out.print("Select: ");
    }

    public static void showMenu() {
        System.out.println("[m] Show main menu");

        System.out.print("Select: ");
    }

    public static void showBye() {
        System.out.println("\nbye");
    }

    public static String toInitCap(String param) {
        if (param != null && param.length() > 0) {
            char[] charArray = param.toCharArray();
            charArray[0] = Character.toUpperCase(charArray[0]);
            // Set capital letter to first position
            return new String(charArray);
            // Return desired output
        } else {
            return "";
        }
    }

    public static void main(String[] args) {
        mainMenu();
        do {
            menuItem = scan.nextLine();

            switch (menuItem) {

                case "1":
                    postgreSql();
                    break;
                case "2":
                    derbyDb();
                    break;
                case "m":
                    mainMenu();
                    break;
                case "q":
                    showBye();
                    break;
                default:
                    showMenu();
                    break;

            }

        } while (!menuItem.equals("q"));

        scan.close();
    }
}
